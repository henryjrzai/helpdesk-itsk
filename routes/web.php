<?php

use App\Models\Pengguna;
use App\Models\UmpanBalik;
use App\Http\Middleware\SuperAdmin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TiketController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\SuperAdmin\UmpBalikController;
use App\Http\Controllers\UmpanBalikController;
use App\Http\Controllers\Admin\ExportController;
use App\Http\Controllers\RiwayatTiketController;
use App\Http\Controllers\Admin\TiketDosenController;
use App\Http\Controllers\SuperAdmin\ExcelController;
use App\Http\Controllers\Teknisi\DownloadController;
use App\Http\Controllers\Admin\TiketKaryawanController;
use App\Http\Controllers\SuperAdmin\PenggunaController;
use App\Http\Controllers\Admin\TiketMahasiswaController;
use App\Http\Controllers\SuperAdmin\UnitKerjaController;
use App\Http\Controllers\Teknisi\TiketBelumProsesController;
use App\Http\Controllers\SuperAdmin\TiketPengaduanController;
use App\Http\Controllers\SuperAdmin\KategoriMasalahController;
use App\Http\Controllers\SuperAdmin\StatusTiketPengaduanController;
use App\Http\Controllers\Admin\DashboardController as DashboardAdmin;
use App\Http\Controllers\Teknisi\DasboardController as DashboardTeknisi;
use App\Http\Controllers\SuperAdmin\DashboardController as DashboardSuperAdmin;
use App\Http\Controllers\Teknisi\AduanSelesaiController;
use App\Http\Controllers\Teknisi\AduanProsesController;
use App\Models\UnitKerja;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*
|--------------------------------------------------------------------------
| Halaman Landing Page
|--------------------------------------------------------------------------
*/
// Login
Route::get('/', [LoginController::class, 'index']);
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/autentikasi', [LoginController::class, 'autentikasi']);
Route::get('/status', function () {
    return view('status');
});

// tampilan dashboard tiap role
Route::get('/dashboard_admin', [LoginController::class, 'admin'])->middleware('Admin');
Route::get('/superadmin/dashboard', [LoginController::class, 'superadmin'])->middleware('SuperAdmin');
Route::get('/dashboard_teknisi', [LoginController::class, 'teknisi'])->middleware('Teknisi');

// logout
Route::post('/logout', [LogoutController::class, 'logout']);

// lupa sandi
Route::get('/lupa_sandi', [LoginController::class, 'lupasandi']);

Route::post('/password_email', [LoginController::class, 'ubahpassword']);

Route::get('/reset-password/{token}', function (string $token) {
    return view('auth.reset-password', ['token' => $token]);
})->name('password.reset');

Route::post('/reset-password', [LoginController::class, 'resetpassword'])->name('password.update');
// Route::post('logout',LogoutController::class)->name('logout');
// Route::get('/', function () {
//     return view('home');
// });
Route::get('/umpan-balik', function () {
    return view('umpan-balik');
});
Route::post('/tambah-umpan-balik', [UmpanBalikController::class, 'tambahUmpanBalik'])
    ->name('tambah.umpan_balik');

/*
|--------------------------------------------------------------------------
| Halaman Form Lapor
|--------------------------------------------------------------------------
*/
// Route::get('/data/unit', [TiketController::class, 'lihatdata'])->name('data-unit');
Route::get('/lapor-{role}', [TiketController::class, 'index'])->name('lapor.index');
Route::post('/lapor-{role}', [TiketController::class, 'store'])->name('lapor.store');

/*
|--------------------------------------------------------------------------
| Halaman Riwayat Tiket
|--------------------------------------------------------------------------
*/
Route::post('/riwayat-tiket', [RiwayatTiketController::class, 'index'])->name('riwayat.index');
/*
|--------------------------------------------------------------------------
| Halaman Super Admin
|--------------------------------------------------------------------------
*/

// Dashboard Super Admin
Route::get('/superadmin/dashboard', [DashboardSuperAdmin::class, 'lihatTiket'])->name('sprdashboard')->middleware('SuperAdmin');
Route::get('/export-excel', [ExcelController::class, 'generateExcel']);

//
Route::get('/superadmin/tiket', function () {
    return view('superadmin.tiket');
})->name('sprtiket')->middleware('SuperAdmin');
Route::get('/superadmin/data-tiket', [TiketPengaduanController::class, 'lihatTiket'])->name('datatiket');
Route::put('/superadmin/tiket/update', [TiketPengaduanController::class, 'update'])->name('tiket.update');
Route::delete('/superadmin/delete', [TiketPengaduanController::class, 'destroy'])->name('tiket.delete');

Route::get('/superadmin/status', [StatusTiketPengaduanController::class, 'index'])->name('sprstatustiket')->middleware('SuperAdmin');

Route::get('/superadmin/kategori-masalah', [KategoriMasalahController::class, 'index'])->name('sprkategorimasalah')->middleware('SuperAdmin');

Route::get('/superadmin/unit', function () {
    return view('superadmin.unit');
})->name('sprunit')->middleware('SuperAdmin');
Route::get('/superadmin/data-unit', [UnitKerjaController::class, 'index'])->name('dataunit');
Route::post('/superadmin/unit/store', [UnitKerjaController::class, 'storeUnit'])->name('unitstore');
Route::delete('/superadmin/unit/delete', [UnitKerjaController::class, 'destroy'])->name('unit.delete');
Route::put('/superadmin/unit/edit', [UnitKerjaController::class, 'update'])->name('unit.update');

Route::get('/superadmin/pengguna', function () {
    return view('superadmin.pengguna');
})->name('sprpengguna')->middleware('SuperAdmin');


Route::get('pengguna', [PenggunaController::class, 'index'])->name('datapengguna');
Route::put('/superadmin', [PenggunaController::class, 'update'])->name('pgn.update');
Route::delete('/superadmin/hapus', [PenggunaController::class, 'destroy'])->name('pgn.delete');
Route::post('/superadmin/store', [PenggunaController::class, 'storePengguna'])->name('pgnstore');

Route::get('/superadmin/umpan-balik', function () {
    return view('superadmin.umpan_balik');
})->name('sprumpanbalik')->middleware('SuperAdmin');

Route::get('/superadmin/data-umpan-balik', [UmpBalikController::class, 'index'])->name('dataumpanbalik');
Route::delete('/superadmin/umpanbalik/delete', [UmpBalikController::class, 'destroy'])->name('umpanbalik.delete');
// Route::get('/superadmin/pengguna', function () {
//     return view('superadmin.pengguna');
// })->name('sprpengguna');

Route::get('/superadmin/profil', function () {
    return view('superadmin.profil');
})->name('sprprofil')->middleware('SuperAdmin');


/*
|--------------------------------------------------------------------------
| Halaman Admin
|--------------------------------------------------------------------------
*/
Route::get('/admin/dashboard', [DashboardAdmin::class, 'lihatTiket'])->name('admdashboard')->middleware('Admin');

Route::get('/generate-excel-admin-dashboard', [ExportController::class, 'generateExcel'])->middleware('Admin');

Route::get('/admin/tiket-mahasiswa', [TiketMahasiswaController::class, 'lihatTiketMahasiswa'])->name('admtiketmahasiswa')->middleware('Admin');
Route::post('/kirim-tiket-mahasiswa', [TiketMahasiswaController::class, 'kirimTiketMahasiswa'])->middleware('Admin');

Route::get('/admin/tiket-dosen', [TiketDosenController::class, 'lihatTiketDosen'])->name('admtiketdosen')->middleware('Admin');
Route::post('/kirim-tiket-dosen', [TiketDosenController::class, 'kirimTiketDosen'])->middleware('Admin');

Route::get('/admin/tiket-karyawan', [TiketKaryawanController::class, 'lihatTiketKaryawan'])->name('admtiketkaryawan')->middleware('Admin');
Route::post('/kirim-tiket-karyawan', [TiketKaryawanController::class, 'kirimTiketKaryawan'])->middleware('Admin');

Route::get('/admin/profil', function () {
    return view('admin.profil');
})->name('admprofil')->middleware('Admin');

Route::get('/admin/umpan-balik', function () {
    return view('admin.umpan_balik');
})->name('admumpanbalik')->middleware('Admin');
Route::get('/admin/data-admin-umpan-balik', [UmpanBalikController::class, 'lihatUmpanBalikAdmin'])->name('dataadmumpanbalik')->middleware('Admin');

Route::post('/admin/umpan-balik/hapus', [UmpanBalikController::class, 'hapusUmpanBalik'])->name('hapusumpanbalik')->middleware('Admin');

/*
|--------------------------------------------------------------------------
| Halaman Teknisi
|--------------------------------------------------------------------------
*/
Route::get('/teknisi/dashboard', [DashboardTeknisi::class, 'lihatTiket'])->name('tknsdashboard')->middleware('Teknisi');

Route::get('/generate-excel', [DownloadController::class, 'generateExcel'])->middleware('Teknisi');
Route::get('/generate-excel-proses', [DownloadController::class, 'generateExcelProses'])->middleware('Teknisi');
Route::get('/generate-excel-selesai', [DownloadController::class, 'generateExcelSelesai'])->middleware('Teknisi');

Route::get('/teknisi/aduan-belum-proses', function () {
    return view('teknisi.aduan_belumproses');
})->name('tksaduanbelumproses')->middleware('Teknisi');
Route::get('/teknisi/data-aduan-belum-proses', [TiketBelumProsesController::class, 'lihatTiketBelumProses'])->name('datatksaduanbelumproses')->middleware('Teknisi');
Route::post('/kirim-tiket-belum-proses', [TiketBelumProsesController::class, 'kirimTiketBelumProses'])->middleware('Teknisi');
Route::get('/download-excel-teknisi-belum-proses', [DownloadController::class, 'downloadExcelTiketBelumProses'])->middleware('Teknisi');

Route::get('/teknisi/aduan-proses', [AduanProsesController::class, 'index'])->name('tksaduanproses');
Route::post('/teknisi/aduan-proses/update', [AduanProsesController::class, 'update'])->middleware('Teknisi');

Route::get('/teknisi/aduan-selesai', [AduanSelesaiController::class, 'index'])->name('tksaduanselesai');

Route::get('/teknisi/profil', function () {
    return view('teknisi.profil');
})->name('tksprofil')->middleware('Teknisi');

/*
|--------------------------------------------------------------------------
| Halaman Authentikasi
|--------------------------------------------------------------------------
*/
// Route::get('/login', function () {
//     return view('auth.login');
// })->name('login');

// Route::get('/lupa-sandi', function () {
//     return view('auth.forgot-password');
// })->name('forgot-password');

Route::get('/reset-sandi', function () {
    return view('auth.reset-password');
})->name('reset-password');

Route::get('/reset-sukses', function () {
    return view('auth.reset-sukses');
})->name('reset-sukses');

/*
|--------------------------------------------------------------------------
| Halaman Profil
|--------------------------------------------------------------------------
*/
Route::post('/ubah-informasi-pribadi-superadmin', [ProfilController::class, 'ubahInformasiPribadi'])->name('ubah.informasi.pribadi.superadmin')->middleware('SuperAdmin');
Route::post('/ubah-informasi-pribadi-admin', [ProfilController::class, 'ubahInformasiPribadi'])->name('ubah.informasi.pribadi.admin')->middleware('Admin');
Route::post('/ubah-informasi-pribadi-teknisi', [ProfilController::class, 'ubahInformasiPribadi'])->name('ubah.informasi.pribadi.teknisi')->middleware('Teknisi');

Route::post('/ubah-informasi-akun-superadmin', [ProfilController::class, 'ubahInformasiAkun'])->name('ubah.informasi.akun.superadmin')->middleware('SuperAdmin');
Route::post('/ubah-informasi-akun-admin', [ProfilController::class, 'ubahInformasiAkun'])->name('ubah.informasi.akun.admin')->middleware('Admin');
Route::post('/ubah-informasi-akun-teknisi', [ProfilController::class, 'ubahInformasiAkun'])->name('ubah.informasi.akun.teknisi')->middleware('Teknisi');

Route::post('/ubah-kata-sandi-superadmin', [ProfilController::class, 'ubahKataSandi'])->name('ubah.kata.sandi.superadmin')->middleware('SuperAdmin');
Route::post('/ubah-kata-sandi-admin', [ProfilController::class, 'ubahKataSandi'])->name('ubah.kata.sandi.admin')->middleware('Admin');
Route::post('/ubah-kata-sandi-teknisi', [ProfilController::class, 'ubahKataSandi'])->name('ubah.kata.sandi.teknisi')->middleware('Teknisi');
