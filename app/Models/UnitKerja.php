<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitKerja extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'unit_kerja';
    protected $primaryKey = 'id';
    protected $fillable = [
        'unit_kerja_karyawan',
    ];

    public function tiket()
    {
        return $this->hasMany(Tiket::class);
    }
}
