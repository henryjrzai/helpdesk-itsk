<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UmpanBalik extends Model
{
    use HasFactory;

    protected $table = 'umpan_balik';
    protected $fillable = [
        'nama',
        'status',
        'email',
        'no_telepon',
        'pesan',
    ];
    
    public $timestamps = false;
}
