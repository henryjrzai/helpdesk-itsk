<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tiket;
use App\Models\LampiranTiket;
use App\Models\RiwayatTiket;
use App\Models\UnitKerja;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;

class TiketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($role)
    {
        $no_tiket = $this->generateTicketNumber();
        $check = Tiket::where('no_tiket', $no_tiket)->first();
        if ($check) {
            $no_tiket = $this->generateTicketNumber();
        }

        $units = UnitKerja::all();

        if ($role == 'dosen') {
            return view('lapor-dosen', ['no_tiket' => $no_tiket, 'units' => $units]);
        } elseif ($role == 'karyawan') {
            return view('lapor-karyawan', ['no_tiket' => $no_tiket, 'units' => $units]);
        } else {
            return view('lapor-mahasiswa', ['no_tiket' => $no_tiket, 'units' => $units]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'namalengkap' => 'required|string|max:100',
                    'email' => 'required|email',
                    'telp' => 'required|string|max:30',
                    'nim' => 'nullable|string|max:50',
                    'prodi' => 'nullable',
                    'nip' => 'nullable|string|max:50',
                    'nik' => 'nullable|string|max:50',
                    'unit' => 'nullable',
                    'subjek' => 'required|string|max:100',
                    'urgensi' => 'required',
                    'kategori' => 'required',
                    'kategori_lain' => 'nullable',
                    'deskripsi' => 'required|string',
                    'tanggal' => 'required|date_format:Y-m-d',
                    'waktu' => 'required|date_format:H:i',
                    'formFile' => 'nullable|file|max:10240', // max 10MB
                ],
                [
                    'namalengkap.required' => 'Nama lengkap wajib diisi.',
                    'namalengkap.max' => 'Nama lengkap maksimal 100 karakter.',
                    'email.required' => 'Email wajib diisi.',
                    'email.email' => 'Email tidak valid.',
                    'telp.required' => 'Nomor telepon wajib diisi.',
                    'telp.max' => 'Nomor telepon maksimal 30 karakter.',
                    'subjek.required' => 'Subjek wajib diisi.',
                    'subjek.max' => 'Subjek maksimal 100 karakter.',
                    'urgensi.required' => 'Tingkat urgensi wajib diisi.',
                    'kategori.required' => 'Kategori laporan wajib diisi.',
                    'deskripsi.required' => 'Deskripsi wajib diisi.',
                    'deskripsi.string' => 'Deskripsi harus berupa teks.',
                    'tanggal.required' => 'Tanggal wajib diisi.',
                    'tanggal.date_format' => 'Format tanggal tidak valid.',
                    'waktu.required' => 'Waktu wajib diisi.',
                    'waktu.date_format' => 'Format waktu tidak valid.',
                    'formFile.file' => 'File harus berupa file.',
                    'formFile.max' => 'File maksimal 10MB.',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
            }

            // $unit = UnitKerja::where('unit_kerja_karyawan', $request->unit)->first();
            // if ($unit) {
            //     $unit_kerja_id = $unit->id;
            // } else {
            //     $unit_kerja_id = null;
            // }
            $tiket = Tiket::create([
                'no_tiket' => $request->no_tiket,
                'nama' => $request->namalengkap,
                'email' => $request->email,
                'no_telepon' => $request->telp,
                'nim_mahasiswa' => $request->nim,
                'prodi' => $request->prodi,
                'nip_dosen' => $request->nip,
                'nik_karyawan' => $request->nik,
                'unit_kerja_id' => $request->unit,
                'judul_tiket' => $request->subjek,
                'tingkat_urgensi' => $request->urgensi,
                'kategori_laporan' => $request->kategori,
                'kategori_lainnya' => $request->kategori_lain,
                'deskripsi_tiket' => $request->deskripsi,
                'waktu_tiket' => Carbon::createFromFormat('Y-m-d H:i', $request->tanggal . ' ' . $request->waktu),
            ]);

            $path = $request->hasFile('formFile')
                ? $request->file('formFile')->store('uploads', 'public')
                : null;

            LampiranTiket::create([
                'id_tiket' => $tiket->id,
                'file_tiket' => $path,
            ]);

            RiwayatTiket::create([
                'id_tiket' => $tiket->id,
                'waktu_riwayat' => now(),
                'deskripsi_riwayat' => 'Laporan sedang diperiksa admin',
            ]);

            return response()->json(['success' => true, 'message' => 'Tiket berhasil disimpan.']);
        } catch (QueryException $e) {
            return response()->json(['success' => false, 'message' => 'Kesalahan Server!, Terjadi kesalahan saat menyimpan data.'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    function generateTicketNumber()
    {
        $tanggal = date('d');
        $bulan = date('m');
        $tahun = substr(date('Y'), -2);

        $angka_acak = mt_rand(1000, 9999);

        $hasil = $tanggal . $bulan . $tahun . $angka_acak;

        return $hasil;
    }
}
