<?php

namespace App\Http\Controllers\SuperAdmin;

use Carbon\Carbon;
use App\Models\Tiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function lihatTiket()
    {
        $now = Carbon::now()->startOfDay();
        $sevenDaysFromNow = Carbon::now()->addDays(7)->endOfDay();

        $data = Tiket::selectRaw('DATE_FORMAT(created_at, "%Y/%m/%d") as date, COUNT(*) as jumlah')
            ->whereNotNull('created_at')
            ->whereBetween('created_at', [$now, $sevenDaysFromNow])
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get();
        

        $jumlahBelumDiproses = Tiket::where('status_tiket', 'Belum Diproses')->count();
        $jumlahSedangDiproses = Tiket::where('status_tiket', 'Sedang Diproses')->count();
        $jumlahSelesai = Tiket::where('status_tiket', 'Selesai')->count();
        $jumlahTeknis = Tiket::where('kategori_laporan', 'Teknis')->count();
        $jumlahAdministratif = Tiket::where('kategori_laporan', 'Administratif')->count();
        $jumlahAkademis = Tiket::where('kategori_laporan', 'Akademis')->count();
        $jumlahLainnya = Tiket::where('kategori_laporan', 'Lainnya')->count();
        $jumlahRendah = Tiket::where('tingkat_urgensi', 'Rendah')->count();
        $jumlahSedang = Tiket::where('tingkat_urgensi', 'Sedang')->count();
        $jumlahTinggi = Tiket::where('tingkat_urgensi', 'Tinggi')->count();

        $tikets = Tiket::with('unit_kerja')->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }
            switch ($tiket->tingkat_urgensi) {
                case 'Rendah':
                    $tiket->tingkat_urgensi = '<span class="badge bg-success">Rendah</span>';
                    break;
                case 'Sedang':
                    $tiket->tingkat_urgensi = '<span class="badge bg-warning">Sedang</span>';
                    break;
                case 'Tinggi':
                    $tiket->tingkat_urgensi = '<span class="badge bg-danger">Tinggi</span>';
                    break;
                default:
                    $tiket->tingkat_urgensi = '---';
            }

            switch ($tiket->status_tiket) {
                case 'Selesai':
                    $tiket->status_tiket = '<span class="badge bg-success">Selesai</span>';
                    break;
                case 'Sedang Diproses':
                    $tiket->status_tiket = '<span class="badge bg-warning">Sedang Diproses</span>';
                    break;
                case 'Belum Diproses':
                    $tiket->status_tiket = '<span class="badge bg-danger">Belum Diproses</span>';
                    break;
                default:
                    $tiket->status_tiket = '---';
            }

            if ($tiket->waktu_tiket !== '---') {
                $tiket->waktu_tiket = date_format(date_create($tiket->waktu_tiket), 'Y/m/d');
            } else {
                $tiket->waktu_tiket = '---';
            }
            
            $tiket->unit_kerja_karyawan = $tiket->unit_kerja ? $tiket->unit_kerja->unit_kerja_karyawan : '---';

            return $tiket;
        });

        foreach ($tikets as $tiket) {
            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }
        }

        return view('superadmin/dashboard', [
            'jumlahBelumDiproses' => $jumlahBelumDiproses,
            'jumlahSedangDiproses' => $jumlahSedangDiproses,
            'jumlahSelesai' => $jumlahSelesai,
            'jumlahTeknis' => $jumlahTeknis,
            'jumlahAdministratif' => $jumlahAdministratif,
            'jumlahAkademis' => $jumlahAkademis,
            'jumlahLainnya' => $jumlahLainnya,
            'jumlahRendah' => $jumlahRendah,
            'jumlahSedang' => $jumlahSedang,
            'jumlahTinggi' => $jumlahTinggi,
            'tikets' => $tikets,
            'data' => $data,
        ]);
    }
}

