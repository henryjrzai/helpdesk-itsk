<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UmpanBalik;
use Yajra\DataTables\Facades\DataTables;

class UmpanBalikController extends Controller
{
    public function tambahUmpanBalik(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:20',
            'status' => 'required|string',
            'email' => 'required|email|max:100',
            'no_telepon' => 'required|string|max:30|regex:/^[0-9]+$/',
            'pesan' => 'required|string',
        ], [
            'nama.required' => 'Input Nama Belum Diisi!',
            'nama.max' => 'Inputan Nama Tidak Boleh Lebih Dari 20 Karakter!',
            'status.required' => 'Input Status Belum Diisi!',
            'email.required' => 'Input Email Belum Diisi!',
            'email.email' => 'Format Email Tidak Valid!',
            'email.max' => 'Inputan Email Tidak Boleh Lebih Dari 100 Karakter!',
            'no_telepon.required' => 'Input Nomor Telepon Belum Diisi!',
            'no_telepon.max' => 'Inputan Nomor Telepon Tidak Boleh Lebih Dari 30 Karakter!',
            'no_telepon.regex' => 'Inputan Nomor Telepon Hanya Boleh Diisi Angka!',
            'pesan.required' => 'Input Pesan Belum Diisi!',
        ]);

        $umpanBalik = UmpanBalik::create([
            'nama' => $request->input('nama'),
            'status' => $request->input('status'),
            'email' => $request->input('email'),
            'no_telepon' => $request->input('no_telepon'),
            'pesan' => $request->input('pesan'),
        ]);

        if ($umpanBalik) {
            return response()->json([
                'success' => true,
                'message' => 'Umpan Balik Terkirim!'
            ]);
        }
    }

    public function hapusUmpanBalik(Request $request)
    {
        $ids = $request->input('ids');
        $deleted = UmpanBalik::whereIn('id', $ids)->delete();

        if ($deleted) {
            return response()->json([
                'success' => true,
                'message' => 'Umpan Balik Berhasil Dihapus!'
            ]);
        }
    }

    public function lihatUmpanBalikAdmin(Request $request)
    {
        if ($request->ajax()) {
            $umpanBalik = UmpanBalik::all();
            return Datatables::of($umpanBalik)->make(true);
        }
    }
}
