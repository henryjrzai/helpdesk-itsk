<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Tiket;
use App\Models\RiwayatTiket;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class TiketBelumProsesController extends Controller
{
    public function lihatTiketBelumProses(Request $request)
    {
        if ($request->ajax()) {
            $teknisiId = Auth::id();

            $tiketBelumProses = Tiket::where('id_pengguna', $teknisiId)
                ->where('status_tiket', 'Belum Diproses')
                ->with('lampiran')
                ->get()
                ->map(function ($item) {
                    $item->tanggal_masuk = Carbon::parse($item->tanggal_masuk)->format('Y/m/d');

                    if ($item->nip_dosen !== null) {
                        $item->posisi = 'Dosen';
                    } elseif ($item->nim_mahasiswa !== null) {
                        $item->posisi = 'Mahasiswa';
                    } elseif ($item->nik_karyawan !== null) {
                        $item->posisi = 'Karyawan';
                    } else {
                        $item->posisi = 'Tidak Diketahui';
                    }

                    if ($item->kategori_laporan === 'Lainnya') {
                        $item->kategori_laporan = $item->kategori_lainnya;
                    }

                    $item->file_tiket = $item->lampiran->first()->file_tiket ?? '---';

                    return $item;
                });
            return Datatables::of($tiketBelumProses)
                ->addColumn('posisi', function ($row) {
                    return $row->posisi;
                })
                ->make(true);
        }
    }

    public function kirimTiketBelumProses(Request $request)
    {
        $teknisiId = Auth::id();
        $tiketId = $request->input('tiketId');
        $tanggalMulaiUnformated = $request->input('tanggalMulai');
        $tanggalMulai = Carbon::createFromFormat('Y-m-d', $tanggalMulaiUnformated)->format('Y-m-d 00:00:01');
        $tanggalSelesaiUnformated = $request->input('tanggalSelesai');
        $tanggalSelesai = Carbon::createFromFormat('Y-m-d', $tanggalSelesaiUnformated)->format('Y-m-d 23:59:59');
        $tiketProses = Tiket::findOrFail($tiketId);

        $tiket = $tiketProses->update([
            'tanggal_pengerjaan' => $tanggalMulai,
            'estimasi_selesai' => $tanggalSelesai,
            'status_tiket' => 'Sedang Diproses',
        ]);

        $riwayatTiket = RiwayatTiket::create([
            'id_pengguna' => $teknisiId,
            'id_tiket' => $tiketProses->id,
            'waktu_riwayat' => now(),
            'deskripsi_riwayat' => 'Laporan sedang ditangani teknisi',
        ]);

        if ($tiket && $riwayatTiket) {
            return response()->json([
                'success' => true,
                'message' => 'Tiket Dikirim Ke Proses!',
            ]);
        }
    }
}
